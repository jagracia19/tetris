unit Tetris.Graphics;

interface

uses
  Tetris.Artifact, Tetris.Game,
  Windows, SysUtils, Types, Controls, Graphics, Math;

type
  TTetrisGraphicControl = class(TGraphicControl)
  private
    FGame: TGame;
    FCellWidth: Integer;
    FCellHeight: Integer;
    FGridWidth: Integer;
    FGridHeight: Integer;
    FGridLeft: Integer;
    FGridTop: Integer;
    function GetColor(Value: Integer): TColor;
    function GetRect(Col, Row: Integer): TRect;
    procedure ResizePlayground;
    procedure DrawPlayground;
    procedure DrawPiece;
    procedure DrawRect(Col, Row: Integer; AColor: TColor);
    function GetPlayground: TPlayground;
    function GetPiece: TPiece;
  protected
    procedure Paint; override;
    property Playground: TPlayground read GetPlayground;
    property Piece: TPiece read GetPiece;
  public
    property Game: TGame read FGame write FGame;
  end;


implementation

const
  PiecesColor: array[TPieceEnum] of TColor = (clBlack, clRed, clBlue, clGreen,
      clYellow, clFuchsia, clTeal, clMaroon);

{ TTetrisGraphicControl }

procedure TTetrisGraphicControl.DrawPiece;
var row   : Integer;
    col   : Integer;
    aColor: TColor;
    v     : Integer;
begin
  if Piece = nil then Exit;
  for row := 0 to Piece.Frame.Height-1 do
    for col := 0 to Piece.Frame.Width-1 do
    begin
      v := Piece.Frame.Cells[col, row];
      if v > 0 then
      begin
        aColor := GetColor(v);
        DrawRect(Piece.Left + col, Piece.Top + row, aColor);
      end;
    end;
end;

procedure TTetrisGraphicControl.DrawPlayground;
var row   : Integer;
    col   : Integer;
    aColor: TColor;
    v     : Integer;
begin
  if Playground = nil then Exit;
  for row := 0 to Playground.Frame.Height-1 do
    for col := 0 to Playground.Frame.Width-1 do
    begin
      v := Playground.Frame.Cells[col, row];
      if v <> 0 then
      begin
        aColor := GetColor(v);
        DrawRect(col, row, aColor);
      end;
    end;
end;

procedure TTetrisGraphicControl.DrawRect(Col, Row: Integer; AColor: TColor);
begin
  Canvas.Brush.Color := AColor;
  Canvas.Rectangle(GetRect(Col, Row));
end;

function TTetrisGraphicControl.GetColor(Value: Integer): TColor;
begin
  if (Value >= Ord(Low(TPieceEnum))) and (Value <= Ord(High(TPieceEnum))) then
    Result := PiecesColor[TPieceEnum(Value)]
  else Result := clBlack;
end;

function TTetrisGraphicControl.GetPiece: TPiece;
begin
  if Game <> nil then
    Result := Game.Piece
  else Result := nil;
end;

function TTetrisGraphicControl.GetPlayground: TPlayground;
begin
  if Game <> nil then
    Result := Game.Playground
  else Result := nil;
end;

function TTetrisGraphicControl.GetRect(Col, Row: Integer): TRect;
begin
  Result.Left := FGridLeft + Col * FCellWidth;
  Result.Top := FGridTop + Row * FCellHeight;
  Result.Right := Result.Left + FCellWidth;
  Result.Bottom := Result.Top + FCellHeight;
end;

procedure TTetrisGraphicControl.Paint;
var st     : string;
    r : TRect;
begin
  inherited;

  Canvas.Brush.Style := bsSolid;
  Canvas.Brush.Color := clBlack;
  Canvas.FillRect(ClientRect);

  if Playground <> nil then
  begin
    ResizePlayground;
    Canvas.Brush.Color := clGray;
    Canvas.Rectangle(FGridLeft, FGridTop, FGridLeft + FGridWidth,
        FGridTop + FGridHeight);
    DrawPlayground;
    DrawPiece;
  end;

  if Game <> nil then
  begin
    Canvas.Brush.Style := bsClear;
    Canvas.Font.Color := clWhite;
    Canvas.Font.Size := 8;
    st := IntToStr(Game.Score);
    r := Rect(FGridLeft, FGridTop, FGridLeft+FGridWidth, FGridTop+FGridHeight);
    DrawText(Canvas.Handle, PChar(st), Length(st), r, DT_CENTER);

    if Game.GameOver then
    begin
      Canvas.Brush.Style := bsClear;
      Canvas.Font.Color := clWhite;
      Canvas.Font.Style := Canvas.Font.Style + [fsBold];
      Canvas.Font.Size := 10;
      st := 'Game Over';
      r := Rect(FGridLeft, FGridTop, FGridLeft+FGridWidth, FGridTop+FGridHeight);
      DrawText(Canvas.Handle, PChar(st), Length(st), r,
          DT_CENTER or DT_VCENTER or DT_SINGLELINE);
    end;
  end;
end;

procedure TTetrisGraphicControl.ResizePlayground;
begin
  if Playground = nil then Exit;
  FCellWidth := Min(ClientWidth div Playground.Frame.Width,
                    ClientHeight div Playground.Frame.Height);
  FCellHeight := FCellWidth;
  FGridWidth := FCellWidth*Playground.Frame.Width;
  FGridHeight := FCellHeight*Playground.Frame.Height;
  FGridLeft := (ClientWidth-FGridWidth) div 2;
  FGridTop := (ClientHeight-FGridHeight) div 2;
end;

end.
