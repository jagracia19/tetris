unit Tetris.Frame;

interface

uses
  Tetris.Graphics, Tetris.Game, Tetris.Artifact,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TFrameTetris = class(TFrame)
    PanelGraphics: TPanel;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
  private
    FTetrisGraphic: TTetrisGraphicControl;
    FGame: TGame;
    procedure GameRepaint(Sender: TObject);
    procedure CreateGraphics;
    function KeyToCommand(Key: Word): TTetrisCommandClass;
  protected
    property TetrisGraphic: TTetrisGraphicControl read FTetrisGraphic;
    property Game: TGame read FGame write FGame;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure KeyDown(Key: Word);
  end;

implementation

uses
  Tetris.Debug;

{$R *.dfm}

{$IFDEF DEBUG}
type
  THackGame = class(TGame)
  end;
{$ENDIF}

{ TFrameTetris }

constructor TFrameTetris.Create(AOwner: TComponent);
begin
  inherited;
  FGame := TGame.Create;
  FGame.OnRepaint := GameRepaint;
  CreateGraphics;
  Timer1.Enabled := True;
end;

procedure TFrameTetris.CreateGraphics;
{$IFDEF DEBUG}
var piece: TPiece;
{$ENDIF}
begin
  FTetrisGraphic := TTetrisGraphicControl.Create(Self);
  FTetrisGraphic.Parent := PanelGraphics;
  FTetrisGraphic.Align := alClient;
  FTetrisGraphic.Game := Game;

  {$IFDEF DEBUG}
//  piece := TPieceS.Create;
//  try
//    piece.SetPosition(Game.Playground.Frame.Width div 2, 1);
//    Game.Playground.Insert(piece.Left, piece.Top, piece.Frame);
//    TTetrisDebug.PrintFrame(TTetrisDebug.GetDirectory + 'playground.txt',
//        Game.Playground.Frame);
//    Game.Piece := piece;
//  finally
//    piece.Free;
//  end;

//  THackGame(Game).CreatePiece;
//
//  piece := TPieceJ.Create;
//  try
//    piece.SetPosition(Game.Playground.Frame.Width div 2,
//                      Game.Playground.Frame.Height-3);
//    Game.Playground.Insert(piece.Left, piece.Top, piece.Frame);
//    TTetrisDebug.PrintFrame(TTetrisDebug.GetDirectory + 'playground.txt',
//        Game.Playground.Frame);
//  finally
//    piece.Free;
//  end;
  {$ENDIF}
end;

destructor TFrameTetris.Destroy;
begin
  FreeAndNil(FGame);
  inherited;
end;

procedure TFrameTetris.GameRepaint(Sender: TObject);
begin
  TetrisGraphic.Repaint;
end;

procedure TFrameTetris.KeyDown(Key: Word);
var command: TTetrisCommandClass;
begin
  command := KeyToCommand(Key);
  if Game.InputCommand(command) then
    TetrisGraphic.Repaint;
end;

function TFrameTetris.KeyToCommand(Key: Word): TTetrisCommandClass;
begin
  case Key of
    VK_LEFT: Result := TMoveLeft;
    VK_RIGHT: Result := TMoveRight;
    VK_DOWN: Result := TMoveDown;
    VK_SPACE: Result := TRotate;
    else Result := nil;
  end;
end;

procedure TFrameTetris.Timer1Timer(Sender: TObject);
begin
  Game.Run;
end;

end.
