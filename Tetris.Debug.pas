unit Tetris.Debug;

interface

uses
  Tetris.Artifact,
  SysUtils, Forms;

type
  TTetrisDebug = class
  public
    class function GetDirectory: string;
    class procedure PrintFrame(const AFilename: string; AFrame: TTetrisArray);
    //procedure PrintPlayground(
  end;

implementation

{ TTetrisDebug }

class function TTetrisDebug.GetDirectory: string;
begin
  Result := IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName)) +
            IncludeTrailingPathDelimiter('log');
end;

class procedure TTetrisDebug.PrintFrame(const AFilename: string;
  AFrame: TTetrisArray);
var F   : TextFile;
    st  : string;
    I, J: Integer;
    v   : Integer;
begin
  AssignFile(F, AFileName);
  if not FileExists(AFileName) then
    Rewrite(F)
  else Append(F);

  for J := 0 to AFrame.Height-1 do
  begin
    st := '';
    for I := 0 to AFrame.Width-1 do
    begin
      v := AFrame.Cells[I, J];
      if (v >= 0) and (v <= 9) then
        st := st + IntToStr(v)
      else st := st + Chr(Ord('A') + v);
    end;
    Writeln(F, st);
  end;

  Flush(F);
  CloseFile(F);
end;

initialization
  ForceDirectories(TTetrisDebug.GetDirectory);

end.
