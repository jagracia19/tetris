program Tetris;

uses
  Forms,
  Main in 'Main.pas' {FormTetris},
  Tetris.Artifact in 'Tetris.Artifact.pas',
  Tetris.Debug in 'Tetris.Debug.pas',
  Tetris.Graphics in 'Tetris.Graphics.pas',
  Tetris.Frame in 'Tetris.Frame.pas' {FrameTetris: TFrame},
  Tetris.Game in 'Tetris.Game.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormTetris, FormTetris);
  Application.Run;
end.
