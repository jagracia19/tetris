unit Tetris.Game;

interface

uses
  Tetris.Artifact,
  Windows, SysUtils, Classes;

type
  TTetrisCommandClass = class of TTetrisCommand;

  TTetrisCommand = class
  private
    FPiece: TPiece;
  public
    class function CreateClass: TTetrisCommand; virtual; abstract;
    procedure Execute; virtual; abstract;
    procedure Undo; virtual; abstract;
    property Piece: TPiece read FPiece write FPiece;
  end;

  TMoveLeft = class(TTetrisCommand)
  public
    class function CreateClass: TTetrisCommand; override;
    procedure Execute; override;
    procedure Undo; override;
  end;

  TMoveRight = class(TTetrisCommand)
  public
    class function CreateClass: TTetrisCommand; override;
    procedure Execute; override;
    procedure Undo; override;
  end;

  TMoveDown = class(TTetrisCommand)
  public
    class function CreateClass: TTetrisCommand; override;
    procedure Execute; override;
    procedure Undo; override;
  end;

  TRotate = class(TTetrisCommand)
  public
    class function CreateClass: TTetrisCommand; override;
    procedure Execute; override;
    procedure Undo; override;
  end;

  TGame = class
  private
    FPlayground: TPlayground;
    FPiece: TPiece;
    FGameOver: Boolean;
    FOnRepaint: TNotifyEvent;
    FTimeDown: Integer;
    FTimeoutDown: Integer;
    FScore: Integer;
    procedure InitArtifacts;
  protected
    procedure DoRepaint;
    procedure CreatePiece;
  public
    constructor Create;
    destructor Destroy; override;
    function InputCommand(ACommandClass: TTetrisCommandClass): Boolean;
    procedure Run;
    property Playground: TPlayground read FPlayground;
    property Piece: TPiece read FPiece;
    property Score: Integer read FScore;
    property GameOver: Boolean read FGameOver;
    property OnRepaint: TNotifyEvent read FOnRepaint write FOnRepaint;
  end;

implementation

{ TGame }

constructor TGame.Create;
begin
  inherited;
  InitArtifacts;
  FTimeDown := GetTickCount;
  FTimeoutDown := 1000;
end;

procedure TGame.CreatePiece;
var pieceTp: TPieceEnum;
begin
  if (Playground = nil) or (Piece <> nil) then Exit;
  pieceTp := TPieceEnum(Random(Ord(High(TPieceEnum))) + 1);
  FPiece := PiecesClass[pieceTp].Make;
  FPiece.SetPosition(Playground.Frame.Width div 2, 1);
end;

destructor TGame.Destroy;
begin
  FreeAndNil(FPiece);
  FreeAndNil(FPlayground);
  inherited;
end;

procedure TGame.DoRepaint;
begin
  if Assigned(FOnRepaint) then FOnRepaint(Self);
end;

procedure TGame.InitArtifacts;
begin
  FPiece := nil;
  FPlayground := TPlayground.Create;
end;

function TGame.InputCommand(ACommandClass: TTetrisCommandClass): Boolean;
var cmd: TTetrisCommand;
begin
  Result := False;
  if Piece = nil then Exit;
  if ACommandClass = nil then Exit;
  cmd := ACommandClass.CreateClass;
  try
    cmd.Piece := Piece;
    cmd.Execute;
    if not Playground.Collide(Piece.Left, Piece.Top, Piece.Frame) then
      Result := True
    else
    begin
      cmd.Undo;
      if cmd is TMoveDown then
      begin
        Playground.Insert(Piece.Left, Piece.Top, Piece.Frame);
        FreeAndNil(FPiece);
        FScore := FScore + Playground.DeleteCompleted;
      end;
    end;
  finally
    cmd.Free;
  end;
end;

procedure TGame.Run;
begin
  if FGameOver then Exit;

  if FPiece = nil then
  begin
    CreatePiece;
    if Playground.Collide(Piece.Left, Piece.Top, Piece.Frame) then
      FGameOver := True;

    DoRepaint;
    Dec(FTimeoutDown, 10);
    FTimeDown := GetTickCount;
  end;

  if GetTickCount - FTimeDown >= FTimeoutDown then
  begin
    InputCommand(TMoveDown);
    FTimeDown := GetTickCount;
    DoRepaint;
  end;
end;

{ TMoveDown }

class function TMoveDown.CreateClass: TTetrisCommand;
begin
  Result := TMoveDown.Create;
end;

procedure TMoveDown.Execute;
begin
  Piece.Top := Piece.Top + 1;
end;

procedure TMoveDown.Undo;
begin
  Piece.Top := Piece.Top - 1;
end;

{ TMoveLeft }

class function TMoveLeft.CreateClass: TTetrisCommand;
begin
  Result := TMoveLeft.Create;
end;

procedure TMoveLeft.Execute;
begin
  Piece.Left := Piece.Left - 1;
end;

procedure TMoveLeft.Undo;
begin
  Piece.Left := Piece.Left + 1;
end;

{ TMoveRight }

class function TMoveRight.CreateClass: TTetrisCommand;
begin
  Result := TMoveRight.Create;
end;

procedure TMoveRight.Execute;
begin
  Piece.Left := Piece.Left + 1;
end;

procedure TMoveRight.Undo;
begin
  Piece.Left := Piece.Left - 1;
end;

{ TRotate }

class function TRotate.CreateClass: TTetrisCommand;
begin
  Result := TRotate.Create;
end;

procedure TRotate.Execute;
begin
  Piece.NextFrame;
end;

procedure TRotate.Undo;
begin
  Piece.PrevFrame;
end;

end.
