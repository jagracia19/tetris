unit Tetris.Artifact;

interface

uses
  SysUtils, Math, Generics.Collections;

type
  TCellVector = array of Integer;

  TTetrisArray = class
  private
    FData: TCellVector;
    FWidth: Integer;
    FHeight: Integer;
    function GetOffset(Col, Row: Integer): Integer;
    function GetCells(Col, Row: Integer): Integer;
    procedure SetCells(Col, Row: Integer; const Value: Integer);
    procedure SetWidth(const Value: Integer);
    procedure SetHeight(const Value: Integer);
  public
    procedure SetSize(AWidth, AHeight: Integer);
    procedure Clear;
    procedure Delete(ARow: Integer; bOnlyPositive: Boolean = False);
    procedure CopyRow(Source, Destine: Integer);
    procedure SetValue(Value: Integer);
    property Width: Integer read FWidth write SetWidth;
    property Height: Integer read FHeight write SetHeight;
    property Cells[Col, Row: Integer]: Integer read GetCells write SetCells;
  end;

  TFrameList = class(TObjectList<TTetrisArray>)
  end;

  TPieceEnum = (pieceNil, pieceS, pieceZ, pieceT, pieceL, pieceJ, pieceO, pieceI);

  TPieceClass = class of TPiece;

  TPiece = class
  private
    FPieceType: TPieceEnum;
    FFrames: TFrameList;
    FFrameIndex: Integer;
    FLeft: Integer;
    FTop: Integer;
    procedure EvaluateFrames;
    function GetFrame: TTetrisArray;
    procedure SetLeft(const Value: Integer);
    procedure SetTop(const Value: Integer);
  protected
    procedure InitFrames; virtual; abstract;
    property Frames: TFrameList read FFrames write FFrames;
    property FrameIndex: Integer read FFrameIndex write FFrameIndex;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    class function Make: TPiece; virtual;
    procedure SetPosition(ALeft, ATop: Integer);
    procedure NextFrame;
    procedure PrevFrame;
    property PieceType: TPieceEnum read FPieceType;
    property Frame: TTetrisArray read GetFrame;
    property Left: Integer read FLeft write SetLeft;
    property Top: Integer read FTop write SetTop;
  end;

  TPieceS = class(TPiece)
  protected
    procedure InitFrames; override;
  public
    constructor Create; override;
    class function Make: TPiece; override;
  end;

  TPieceZ = class(TPiece)
  protected
    procedure InitFrames; override;
  public
    constructor Create; override;
    class function Make: TPiece; override;
  end;

  TPieceT = class(TPiece)
  protected
    procedure InitFrames; override;
  public
    constructor Create; override;
    class function Make: TPiece; override;
  end;

  TPieceL = class(TPiece)
  protected
    procedure InitFrames; override;
  public
    constructor Create; override;
    class function Make: TPiece; override;
  end;

  TPieceJ = class(TPiece)
  protected
    procedure InitFrames; override;
  public
    constructor Create; override;
    class function Make: TPiece; override;
  end;

  TPieceO = class(TPiece)
  protected
    procedure InitFrames; override;
  public
    constructor Create; override;
    class function Make: TPiece; override;
  end;

  TPieceI = class(TPiece)
  protected
    procedure InitFrames; override;
  public
    constructor Create; override;
    class function Make: TPiece; override;
  end;

  TPlayground = class
  private
    FFrame: TTetrisArray;
  protected
    procedure InitFrame;
  public
    constructor Create;
    destructor Destroy; override;
    function Collide(ALeft, ATop: Integer; AFrame: TTetrisArray): Boolean;
    procedure Insert(ALeft, ATop: Integer; AFrame: TTetrisArray);
    function DeleteCompleted: Integer;
    property Frame: TTetrisArray read FFrame;
  end;


const
  PiecesClass: array[TPieceEnum] of TPieceClass = (TPiece, TPieceS, TPieceZ,
      TPieceT, TPieceL, TPieceJ, TPieceO, TPieceI);


implementation

uses
  Tetris.Debug;

{ TTetrisArray }

procedure TTetrisArray.Clear;
var I, N: Integer;
begin
  if FData = nil then Exit;
  N := Width*Height;
  for I := 0 to N-1 do
    FData[I] := 0;
end;

procedure TTetrisArray.CopyRow(Source, Destine: Integer);
var ofsSrc  : Integer;
    ofsDest : Integer;
    I       : Integer;
begin
  ofsSrc := GetOffset(0, Source);
  ofsDest := GetOffset(0, Destine);
  for I := 0 to Width-1 do
  begin
    FData[ofsDest] := FData[ofsSrc];
    Inc(ofsDest);
    Inc(ofsSrc);
  end;
end;

procedure TTetrisArray.Delete(ARow: Integer; bOnlyPositive: Boolean);
var ofs : Integer;
    I   : Integer;
begin
  if FData = nil then Exit;
  ofs := GetOffset(0, ARow);
  for I := 0 to Width-1 do
  begin
    if not bOnlyPositive or (FData[ofs] > 0) then
      FData[ofs] := 0;
    Inc(ofs);
  end;
end;

function TTetrisArray.GetCells(Col, Row: Integer): Integer;
begin
  Result := FData[GetOffset(Col, Row)];
end;

function TTetrisArray.GetOffset(Col, Row: Integer): Integer;
begin
  Result := Row * FWidth + Col;
end;

procedure TTetrisArray.SetCells(Col, Row: Integer; const Value: Integer);
begin
  FData[GetOffset(Col, Row)] := Value;
end;

procedure TTetrisArray.SetHeight(const Value: Integer);
begin
  if Value <= 0 then
    raise Exception.Create('Invalid height value');
  SetSize(Width, Value);
end;

procedure TTetrisArray.SetSize(AWidth, AHeight: Integer);
begin
  if (FWidth <> AWidth) or (FHeight <> AHeight)  then
  begin
    FWidth := AWidth;
    FHeight := AHeight;
    SetLength(FData, FWidth*FHeight);
  end;
end;

procedure TTetrisArray.SetValue(Value: Integer);
var I, J: Integer;
begin
  for J := 0 to Height-1 do
    for I := 0 to Width-1 do
      if  Cells[I, J] <> 0 then
        Cells[I, J] := Value;
end;

procedure TTetrisArray.SetWidth(const Value: Integer);
begin
  if Value <= 0 then
    raise Exception.Create('Invalid height value');
  SetSize(Value, Height);
end;

{ TPiece }

constructor TPiece.Create;
begin
  inherited;
  FFrames := TFrameList.Create(True);
  InitFrames;
  EvaluateFrames;
end;

destructor TPiece.Destroy;
begin
  FreeAndNil(FFrames);
  inherited;
end;

procedure TPiece.EvaluateFrames;
var ixFrame : Integer;
    aFrame  : TTetrisArray;
    I, J    : Integer;
begin
  for ixFrame := 0 to FFrames.Count-1 do
  begin
    aFrame := FFrames[ixFrame];
    for J := 0 to aFrame.Height-1 do
      for I := 0 to aFrame.Width-1 do
        if aFrame.Cells[I, J] <> 0 then
          aFrame.Cells[I, J] := Ord(PieceType);
  end;
end;

function TPiece.GetFrame: TTetrisArray;
begin
  Result := Frames[FrameIndex];
end;

class function TPiece.Make: TPiece;
begin
  Result := nil;
end;

procedure TPiece.NextFrame;
begin
  if FFrameIndex < FFrames.Count-1 then
    Inc(FFrameIndex)
  else FFrameIndex := 0;
end;

procedure TPiece.PrevFrame;
begin
  if FFrameIndex > 0 then
    Dec(FFrameIndex)
  else FFrameIndex := FFrames.Count-1;
end;

procedure TPiece.SetLeft(const Value: Integer);
begin
  SetPosition(Value, Top);
end;

procedure TPiece.SetPosition(ALeft, ATop: Integer);
begin
  if (FLeft <> ALeft) or (FTop <> ATop) then
  begin
    FLeft := ALeft;
    FTop := ATop;
  end;
end;

procedure TPiece.SetTop(const Value: Integer);
begin
  SetPosition(Left, Value);
end;

{ TPieceS }

constructor TPieceS.Create;
begin
  FPieceType := pieceS;
  inherited;
end;

procedure TPieceS.InitFrames;
var f: TTetrisArray;
begin
  inherited;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 1;   f.Cells[1, 0] := 0;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 0;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 1;   f.Cells[2, 2] := 0;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 1;   f.Cells[2, 0] := 1;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 0;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;
end;

class function TPieceS.Make: TPiece;
begin
  Result := TPieceS.Create;
end;

{ TPieceZ }

constructor TPieceZ.Create;
begin
  FPieceType := pieceZ;
  inherited;
end;

procedure TPieceZ.InitFrames;
var f: TTetrisArray;
begin
  inherited;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 1;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 0;
  f.Cells[0, 2] := 1;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 1;   f.Cells[1, 0] := 1;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 0;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;
end;

class function TPieceZ.Make: TPiece;
begin
  Result := TPieceZ.Create;
end;

{ TPieceT }

constructor TPieceT.Create;
begin
  FPieceType := pieceT;
  inherited;
end;

procedure TPieceT.InitFrames;
var f: TTetrisArray;
begin
  inherited;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 1;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 1;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 0;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 1;   f.Cells[2, 2] := 0;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 0;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 1;   f.Cells[2, 2] := 0;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 1;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 0;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 1;   f.Cells[2, 2] := 0;
end;

class function TPieceT.Make: TPiece;
begin
  Result := TPieceT.Create;
end;

{ TPieceL }

constructor TPieceL.Create;
begin
  FPieceType := pieceL;
  inherited;
end;

procedure TPieceL.InitFrames;
var f: TTetrisArray;
begin
  inherited;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 0;   f.Cells[2, 0] := 1;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 1;   f.Cells[2, 0] := 1;
  f.Cells[0, 1] := 0;   f.Cells[1, 1] := 0;   f.Cells[2, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 1;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 1;   f.Cells[1, 0] := 1;   f.Cells[2, 0] := 1;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 0;   f.Cells[2, 1] := 0;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 1;   f.Cells[1, 0] := 0;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 0;   f.Cells[2, 1] := 0;
  f.Cells[0, 2] := 1;   f.Cells[1, 2] := 1;   f.Cells[2, 2] := 0;
end;

class function TPieceL.Make: TPiece;
begin
  Result := TPieceL.Create;
end;

{ TPieceJ }

constructor TPieceJ.Create;
begin
  FPieceType := pieceJ;
  inherited;
end;

procedure TPieceJ.InitFrames;
var f: TTetrisArray;
begin
  inherited;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 1;   f.Cells[1, 0] := 0;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 0;   f.Cells[2, 0] := 1;
  f.Cells[0, 1] := 0;   f.Cells[1, 1] := 0;   f.Cells[2, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 1;   f.Cells[2, 2] := 1;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 0;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 1;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(3, 3);
  f.Cells[0, 0] := 1;   f.Cells[1, 0] := 1;   f.Cells[2, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 0;   f.Cells[2, 1] := 0;
  f.Cells[0, 2] := 1;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;
end;

class function TPieceJ.Make: TPiece;
begin
  Result := TPieceJ.Create;
end;

{ TPieceO }

constructor TPieceO.Create;
begin
  FPieceType := pieceO;
  inherited;
end;

procedure TPieceO.InitFrames;
var f: TTetrisArray;
begin
  inherited;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(2, 2);
  f.Cells[0, 0] := 1;   f.Cells[1, 0] := 1;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;
end;

class function TPieceO.Make: TPiece;
begin
  Result := TPieceO.Create;
end;

{ TPieceI }

constructor TPieceI.Create;
begin
  FPieceType := pieceI;
  inherited;
end;

procedure TPieceI.InitFrames;
var f: TTetrisArray;
begin
  inherited;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(4, 4);
  f.Cells[0, 0] := 1;   f.Cells[1, 0] := 0;   f.Cells[2, 0] := 0;   f.Cells[3, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 0;   f.Cells[2, 1] := 0;   f.Cells[3, 1] := 0;
  f.Cells[0, 2] := 1;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;   f.Cells[3, 2] := 0;
  f.Cells[0, 3] := 1;   f.Cells[1, 3] := 0;   f.Cells[2, 3] := 0;   f.Cells[3, 3] := 0;

  f := TTetrisArray.Create;
  Frames.Add(f);
  f.SetSize(4, 4);
  f.Cells[0, 0] := 0;   f.Cells[1, 0] := 0;   f.Cells[2, 0] := 0;   f.Cells[3, 0] := 0;
  f.Cells[0, 1] := 1;   f.Cells[1, 1] := 1;   f.Cells[2, 1] := 1;   f.Cells[3, 1] := 1;
  f.Cells[0, 2] := 0;   f.Cells[1, 2] := 0;   f.Cells[2, 2] := 0;   f.Cells[3, 2] := 0;
  f.Cells[0, 3] := 0;   f.Cells[1, 3] := 0;   f.Cells[2, 3] := 0;   f.Cells[3, 3] := 0;
end;

class function TPieceI.Make: TPiece;
begin
  Result := TPieceI.Create;
end;

{ TPlayground }

function TPlayground.Collide(ALeft, ATop: Integer; AFrame: TTetrisArray): Boolean;
var I, J: Integer;
begin
  Result := False;
  for J := 0 to AFrame.Height-1 do
    for I := 0 to AFrame.Width-1 do
      if (AFrame.Cells[I, J] <> 0) and
         (Frame.Cells[ALeft+I, ATop+J] <> 0)
      then Exit(True);
end;

constructor TPlayground.Create;
begin
  inherited;
  FFrame := TTetrisArray.Create;
  InitFrame;
end;

function TPlayground.DeleteCompleted: Integer;
var I, J, K   : Integer;
    value     : Integer;
    anyValue  : Integer;
    completed : Boolean;
begin
  Result := 0;
  J := 0;
  while J < Frame.Height do
  begin
    // check if row is completed
    anyValue := 0;
    completed := True;
    for I := 0 to Frame.Width-1 do
    begin
      value := Frame.Cells[I, J];
      if value = 0 then
        completed := False
      else if value > 0 then
        anyValue := value;
    end;
    if completed and (anyValue <> 0) then
    begin
      Inc(Result);

      // mover rows down
      for K := J-1 downto 1 do
        Frame.CopyRow(K, K+1);
      Frame.Delete(1, True);
    end
    else Inc(J);
  end;
end;

destructor TPlayground.Destroy;
begin
  FreeAndNil(FFrame);
  inherited;
end;

procedure TPlayground.InitFrame;
var I: Integer;
begin
  Frame.SetSize(12, 22);
  Frame.Clear;

  // condicion de contorno
  for I := 0 to Frame.Height-1 do
  begin
    Frame.Cells[0, I] := -1;
    Frame.Cells[Frame.Width-1, I] := -1;
  end;
  for I := 0 to Frame.Width-1 do
  begin
    Frame.Cells[I, 0] := -1;
    Frame.Cells[I, Frame.Height-1] := -1;
  end;
end;

procedure TPlayground.Insert(ALeft, ATop: Integer; AFrame: TTetrisArray);
var I, J: Integer;
begin
  for J := 0 to AFrame.Height-1 do
    for I := 0 to AFrame.Width-1 do
      if AFrame.Cells[I, J] <> 0 then
         Frame.Cells[ALeft+I, ATop+J] := AFrame.Cells[I, J];
end;

end.
