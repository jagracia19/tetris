unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Tetris.Frame;

type
  TFormTetris = class(TForm)
    FrameTetris: TFrameTetris;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
  public
  end;

var
  FormTetris: TFormTetris;

implementation

{$R *.dfm}

procedure TFormTetris.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  FrameTetris.KeyDown(Key);
end;

end.
